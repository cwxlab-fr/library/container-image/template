#!/usr/bin/env sh

set -eu

ACTION="${1:-build}"
CT_BIN="${CT_BIN:-podman}"
command -v "${CT_BIN}" || CT_BIN="docker"

IMAGE="${IMAGE:-template}"
REGISTRY="${REGISTRY:-registry.gitlab.com/cwxlab-fr/library/container-image}"
CONTEXT="${CONTEXT:-docker}"
TAGS="${TAGS:-latest}"

TAG_LIST=""
for tag in ${TAGS} ; do
    TAG_LIST="${TAG_LIST} --tag ${REGISTRY}/${IMAGE}:${tag}"
done

case ${ACTION} in
    build)
        # shellcheck disable=SC2086
        podman build --build-arg=REGISTRY="${REGISTRY}" ${TAG_LIST} "${CONTEXT}"
    ;;
    push)
        for tag in ${TAGS} ; do
            podman push "${REGISTRY}/${IMAGE}:${tag}"
        done
    ;;
    *)
        echo "Invalid action: ${ACTION}"
        exit 1
    ;;
esac
